﻿using UnityEngine;

public class DefaultAction : MonoBehaviour, IActionable {

    void IActionable.Act()
    {
        Debug.Log(name + " Acted");
    }
}
