﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventWithArgs : UnityEvent<Dictionary<int, int>> { }

public class EventManager : MonoBehaviour {

    private Dictionary<int, EventWithArgs> _eventDictionary;
    private static EventManager _eventManager;

    public static EventManager Instance
    {
        get
        {
            if(!_eventManager)
            {
                _eventManager = FindObjectOfType(typeof(EventManager)) as EventManager;

                if(!_eventManager)
                {
                    Debug.LogError("There needs to be one active EventManager script in the scene");
                }
                else
                {
                    // ReSharper disable once PossibleNullReferenceException
                    _eventManager.Init();
                }
            }

            return _eventManager;
        }
    }

    private void Init()
    {
        if(_eventDictionary == null)
        {
            _eventDictionary = new Dictionary<int, EventWithArgs>();
        }
    }

    public static void StartListening(int eventName, UnityAction<Dictionary<int, int>> listener)
    {
        EventWithArgs thisEvent;

        if(Instance._eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.AddListener(listener);
        }
        else
        {
            thisEvent = new EventWithArgs();
            thisEvent.AddListener(listener);
            Instance._eventDictionary.Add(eventName, thisEvent);
        }
    }

    public static void StopListening(int eventName, UnityAction<Dictionary<int, int>> listener)
    {
        if (_eventManager == null) return;

        EventWithArgs thisEvent;
        if(Instance._eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.RemoveListener(listener);
        }
    }

    public static void TriggerEvent(int eventName, Dictionary<int, int> eventParams)
    {
        EventWithArgs thisEvent;
        if(Instance._eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.Invoke(eventParams);
        }
    }

}
