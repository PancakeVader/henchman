﻿using UnityEngine;

/// <summary>
/// DeckManager handles adding and removing cards
/// from the deck as well as handling extra cards
/// when player's hand is full
/// </summary>
public class DeckManager : MonoBehaviour {

    //Simple test for dealing cards from deck
    private void Update()
    {
        if(Input.GetButtonDown("Jump"))
        {
            if(Hand.CardCount < 10)
            {
                Hand.AddCard(Deck.DealCard());
            }
            else
            {
                //TODO: Handle discarding extra card
            }
        }
    }

    public Deck Deck;
    public CardContainer Hand;
}
