﻿using System.Collections.Generic;
using System.Security.Cryptography;

/// <summary>
/// Extends IList with the ability to shuffle the list.
/// </summary>
public static class Extensions
{
    // Shuffles the element order of the specified list.
    public static void Shuffle<T>(this IList<T> list)
    {
        var provider = new RNGCryptoServiceProvider();
        int n = list.Count;
        while (n > 1)
        {
            var box = new byte[1];
            do provider.GetBytes(box);
            while (!(box[0] < n * (byte.MaxValue / n)));
            var k = box[0] % n;
            n--;
            var value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}

