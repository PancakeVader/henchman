﻿using System.Collections.Generic;
using UnityEngine;

public class BasicEnemy : MonoBehaviour, IActionable {

    private EnemyObject _thisEnemy;

    private void OnEnable()
    {
        _thisEnemy = GetComponent<EnemyObject>();
    }

    void IActionable.Act()
    {
        if(_thisEnemy != null)
        {
            EventManager.TriggerEvent(Events.ChangeHealth,
            new Dictionary<int, int> { { Events.ChangeHealth, _thisEnemy.Damage } });
        }
        
    }
}