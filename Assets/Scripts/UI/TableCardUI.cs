﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TableCardUi : MonoBehaviour {

    //References set in Unity Inspector
    public Text ToHitText;
    public CardContainer TableCards;

    private int _totalModifier;


	// Use this for initialization
    private void Start () {
        _totalModifier = 0;
	}

    private void OnEnable()
    {
        EventManager.StartListening(Events.TableCardsUpdated, CalculateTableCards);
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.TableCardsUpdated, CalculateTableCards);
    }

    private void CalculateTableCards(Dictionary<int, int> eventParams)
    {
        _totalModifier = 0;
        foreach (GameObject obj in TableCards.GetAllCards())
        {
            //TODO
            //_totalModifier += obj.GetComponent<CardObject>().CardData.ModifierAmount;
        }
        ToHitText.text = _totalModifier.ToString("+#;-#;0");
    }
}
