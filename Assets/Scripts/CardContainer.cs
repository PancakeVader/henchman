﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// CardContainer stores GameObjects and provides methods
/// to add, remove and move GameObjects between other
/// CardContainers
/// </summary>
public class CardContainer : MonoBehaviour {

    [SerializeField]
    private List<GameObject> _cardsInContainer;

    public Transform ContainerPosition;
    public int CardCount;
    public CardContainer OtherContainer;

    public bool IsTable;

    // Use this for initialization
    private void Start()
    {
        _cardsInContainer = new List<GameObject>();
        CardCount = 0;
    }

    public void AddCard(GameObject cardToAdd)
    {
        if (cardToAdd != null)
        {
            _cardsInContainer.Add(cardToAdd);
            cardToAdd.transform.SetParent(transform);
            ReorderCards();
        }
    }

    public void MoveCard(GameObject cardPlayed)
    {
        if (OtherContainer == null)
        {
            Debug.LogError("otherContainer = null! No Card played");
        }
        else
        {
            OtherContainer.AddCard(cardPlayed);
            _cardsInContainer.Remove(cardPlayed);
        }

        ReorderCards();
    }

    public List<GameObject> GetAllCards()
    {
        return _cardsInContainer;
    }

    public void ClearContainer()
    {
        foreach(GameObject go in _cardsInContainer)
        {
            Destroy(go);
        }
        _cardsInContainer.Clear();

        if (IsTable)
        {
            EventManager.TriggerEvent(Events.TableCardsUpdated, null);
        }
    }

    private void ReorderCards()
    {
        CardCount = _cardsInContainer.Count;

        Vector3 offset = new Vector3(1.5f, 0f, 0.1f);

        //We calculate the position for the left most card.
        // ReSharper disable once PossibleLossOfFraction
        Vector3 startingLocation = -(offset * (CardCount / 2)) + ContainerPosition.position;

        // Center of even cards isn't on origo so we add half the offset to center
        // everything properly
        if (CardCount % 2 == 0)
            startingLocation += offset / 2;

        for (int i = 0; i < CardCount; i++)
        {
            _cardsInContainer[i].transform.position = startingLocation + (offset * i);
        }


        if (IsTable)
        {
            EventManager.TriggerEvent(Events.TableCardsUpdated, null);
        }
    }

}
