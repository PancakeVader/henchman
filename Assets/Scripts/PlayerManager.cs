﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

    //Player attributes
    public int Health { get; set; }
    public int TotalArmor { get; set; }
    public int TotalDamage { get; set; }
    public int Accuracy { get; set; }
    public int Evade { get; set; }

    private void Awake()
    {
        EventManager.StartListening(Events.ChangeHealth, TakeDamage);
    }

    private void Start ()
    {
        Health = 10;
        TotalArmor = 10;
        TotalDamage = 40;
        Accuracy = 30;
        Evade = 10;
	}

    public void TakeDamage(Dictionary<int, int> eventParams)
    {
        if(eventParams != null)
        {
            int damage;
            eventParams.TryGetValue(Events.ChangeHealth, out damage);
            if(damage > TotalArmor)
                Health -= damage - TotalArmor;
        }
        else
        {
            Debug.LogError("eventParams not passed to PlayerManager.TakeDamage(). Check event trigger");
        }
        //Check to see if player is dead
        if (Health <= 0)
            Die();
    }

    void Die()
    {
        Debug.Log("Player has died");
        EventManager.TriggerEvent(Events.PlayerDied, null);
    }
}
