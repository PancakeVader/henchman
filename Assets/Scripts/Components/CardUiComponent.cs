﻿using UnityEngine;
using UnityEngine.UI;

public class CardUiComponent : MonoBehaviour {

    ////UI Elements
    [SerializeField]
    private Text _descriptionText;
    [SerializeField]
    private Text _nameText;
    [SerializeField]
    private Text _typeText;

    private CardObject _card;


    private void Start()
    {
        _card = GetComponent<CardObject>();
        _nameText.text = _card.CardData.Name;
        _descriptionText.text = _card.CardData.Description;
        _typeText.text = _card.CardData.CardType.ToString();
    }
}
