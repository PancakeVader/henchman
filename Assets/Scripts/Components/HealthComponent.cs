﻿using UnityEngine;
using UnityEngine.Events;


public class HealthComponent : MonoBehaviour {

    public int Health { get; set; }
    public int TotalArmor { get; set; }

    public delegate void HealthChanged(int health);
    public event HealthChanged OnHealthChanged;


    private void Start()
    {
        Health = 10;
        TotalArmor = 0;
    }

    private void ChangeHealth(int amount)
    {
        Health += amount;
        OnHealthChanged(Health);
    }
}
