﻿using UnityEngine;

public class DeathComponent : MonoBehaviour {

    private void Start()
    {
        GetComponentInParent<HealthComponent>().OnHealthChanged += CheckIfDead;
    }

    private void CheckIfDead(int currentHealth)
    {
        if (currentHealth <= 0)
            Die();
    }

    private void Die()
    {
        Debug.Log(gameObject.name + " has died");
    }
}
