﻿using UnityEngine;


/// <summary>
/// GameManager tracks the game phase
/// </summary>
enum Phase
{
    PlayerTurn = 0,
    Resolve = 1,
    AiTurn = 2
}


public class GameManager : MonoBehaviour {

    //References to scripts this class drives
    public ResolvePlayerTurn ResolveScript;
    public AiTurn AiTurnScript;

    private Phase _currentPhase;

    private void Start () {
        _currentPhase = Phase.PlayerTurn;
	}


    private void Update () {
		
        if(Input.GetKeyDown("q"))
        {
            _currentPhase = Phase.Resolve;
        }

        switch (_currentPhase)
        {
            case Phase.PlayerTurn:
                {
                    EventManager.TriggerEvent(Events.EnableMouseInteraction, null);
                    break;
                }
            case Phase.Resolve:
                {
                    //EventManager.TriggerEvent("DisableMouseInteraction", null);
                    ResolveScript.ResolveTurn();
                    _currentPhase = Phase.AiTurn;
                    break;
                }
            case Phase.AiTurn:
                {
                    //TODO: Handle AI turn
                    AiTurnScript.ResolveAiTurn();
                    _currentPhase = Phase.PlayerTurn;
                    break;
                }
        }
	}
}
