﻿using System.Collections.Generic;
using UnityEngine;

public class ResolvePlayerTurn : MonoBehaviour {

    //References set in the inspector
    public EnemySpawner EnemySpawner;
    public PlayerManager Player;

    [SerializeField]
    private CardContainer _tableCards;

    [SerializeField]
    private int _diceRoll;

    private EnemyObject _targetEnemy;
    private int _targetSlot;

    private void Awake()
    {
        EventManager.StartListening(Events.ChangeTarget, ChangeTarget);
        _targetSlot = -1;
    }

    public void ResolveTurn()
    {
        //Make sure we have a valid target
        if(_targetSlot < 0)
        {
            _targetSlot = 0;
            _targetEnemy = EnemySpawner.SpawnedEnemies[_targetSlot].GetComponent<EnemyObject>();
        }

        if(_targetEnemy != null)
        {
            //roll a die first
            _diceRoll = Random.Range(1, 20);

            //Resolve which enemy to attack and if we hit them
            if (_diceRoll + Player.Accuracy >= _targetEnemy.Evade)
            {
                if (Player.TotalDamage > _targetEnemy.Armor)
                {
                    _targetEnemy.TakeDamage(Player.TotalDamage - _targetEnemy.Armor);
                }
            }

            foreach (GameObject go in _tableCards.GetAllCards())
            {
                go.GetComponent<IActionable>().Act();
            }
            _tableCards.ClearContainer();
        }
        else
        {
            Debug.Log("targetEnemy is null. Could not resolve combat");
        }
    }

    private void ChangeTarget(Dictionary<int, int> eventParams)
    {
        if (eventParams != null)
        {
            if(eventParams.TryGetValue(Events.ChangeTarget, out _targetSlot))
                _targetEnemy = EnemySpawner.SpawnedEnemies[_targetSlot].GetComponent<EnemyObject>();

        }
    }
}
