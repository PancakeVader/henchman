﻿using UnityEngine;

/// <summary>
/// PreviewCard class handles resizing and relocating the cards for
/// a closer "zoomed in" look
/// </summary>
public class PreviewCard : MonoBehaviour {

    //References set in Unity Inspector
    private Transform _previewObject;

    private Vector3 _originalPosition;
    private Vector3 _originalScale;

    public void StartPreview(Transform cardTransform)
    {
            _previewObject = cardTransform;
            _originalPosition = _previewObject.transform.position;
            _originalScale = _previewObject.transform.localScale;

            _previewObject.transform.position = new Vector3(0f, -1f, -2f);
            _previewObject.transform.localScale *= 2;
    }

    public void EndPreview()
    {
        _previewObject.transform.position = _originalPosition;
        _previewObject.transform.localScale = _originalScale;
    }
}
