﻿using UnityEngine;

public enum CardType
{
    Spell,
    Modifier,
    Skill
}

/// <summary>
/// CardAsset is used to store information needed to create a card
/// CardAssets can be created from the Create-menu
/// </summary>
[CreateAssetMenu(fileName = "Data", menuName = "Cards/CardAsset", order = 1)]
public class CardAsset : ScriptableObject {

    [Header("Card Info")]
    public string Name;
    [TextArea (2,3)]
    public string Description;
    public Sprite CardImage;
    public string ActionScriptName;
    public CardType CardType;
    public int ModifierAmount;
    
}