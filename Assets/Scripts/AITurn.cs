﻿using UnityEngine;

public class AiTurn : MonoBehaviour {

    [SerializeField]
    private EnemySpawner _enemies;

    public void ResolveAiTurn()
    {
        foreach (GameObject go in _enemies.SpawnedEnemies)
        {
            go.GetComponent<IActionable>().Act();
        }
    }
}