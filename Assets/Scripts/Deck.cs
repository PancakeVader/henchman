﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Contains all the cards in the deck. We only store
/// CardAssets as they contain all the data needed
/// to instanciate a card object. Deck is managed by
/// DeckManager class
/// </summary>
public class Deck : MonoBehaviour {



    private void Awake()
    {
        CardCount = CardList.Count;
        if(CardCount > 0)
            CardList.Shuffle();
    }


    public GameObject DealCard()
    {
        if (CardCount > 0)
        {
            CardAsset nextCard = CardList[CardCount - 1];
            CardList.RemoveAt(CardCount - 1);
            CardCount--;

            return CardBuilder.Instance.BuildCard(nextCard);
        }

        return null;
    }

    public List<CardAsset> CardList;
    public int CardCount;
    public GameObject CardPrefab;
}
