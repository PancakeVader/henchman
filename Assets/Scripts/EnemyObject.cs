﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handles setting up the enemy GameObject in the scene
/// </summary>
public class EnemyObject : MonoBehaviour {


    //Properties
    public EnemyProfession Profession { get; set; }
    public int Health { get; set; }
    public int Armor { get; set; }
    public int Damage { get; set; }
    public int Accuracy { get; set; }
    public int Evade { get; set; }
    public string Name { get; set; }
    public Sprite SpriteImage { get; set; }
    public int Id { get; set; }

    //UI elements
    public Text NameText;
    public Text TypeText;

    public void TakeDamage(int damageTaken)
    {
        if(Armor < damageTaken)
            Health -= damageTaken - Armor;

        if(Health <= 0)
        {
            //Handle Death
        }
    }

    public void SetGraphics()
    {
        GetComponentInChildren<Image>().sprite = SpriteImage;
        NameText.text = Name;
        TypeText.text = Profession.ToString();
    }
}
