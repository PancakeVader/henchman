﻿using UnityEngine;

/// <summary>
/// EnemyBuilder builds the enemy GameObjects from EnemyAsset
/// and sets up the action script and enemy stats
/// </summary>
public class EnemyBuilder : MonoBehaviour {

	public static void BuildEnemy(EnemyAsset enemyAsset, GameObject enemy)
    {
        //Set up the GameObject
        EnemyObject enemyObject = enemy.GetComponent<EnemyObject>();
        enemy.name = enemyAsset.name;

        //Copy values from asset to object
        enemyObject.Name = enemyAsset.Name;
        enemyObject.Health = enemyAsset.Health;
        enemyObject.Armor = enemyAsset.Armor;
        enemyObject.Damage = enemyAsset.Damage;
        enemyObject.Accuracy = enemyAsset.Accuracy;
        enemyObject.Evade = enemyAsset.Evade;
        enemyObject.SpriteImage = enemyAsset.Sprite;
        enemyObject.Profession = enemyAsset.Profession;
        
        enemyObject.SetGraphics();

        // We take a type reference of the type named in ActionScriptName
        System.Type type = System.Type.GetType(enemyAsset.ActionScriptName);

        // Throws a warning if Unity can't find the script named in ActionScriptName
        if (type != null)
        {
            enemy.AddComponent(type);
        }
        else
        {
            Debug.LogWarning("Action Script not found. Default script used");
            enemy.AddComponent(System.Type.GetType("BasicKnight"));
        }
    }
}
