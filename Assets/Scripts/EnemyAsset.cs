﻿using UnityEngine;

public enum EnemyProfession
{
    Rogue,
    Knight,
    Cleric
}

/// <summary>
/// EnemyAsset stores information about an enemy character/creature
/// It is used to create Enemy object in the scene
/// </summary>

[CreateAssetMenu(fileName = "EnemyData", menuName = "Enemies/EnemyAsset", order = 1)]
public class EnemyAsset : ScriptableObject {

    public EnemyProfession Profession;

    public int Health;
    public int Armor;
    public int Damage;
    public int Accuracy;
    public int Evade;
    public string Name;
    public Sprite Sprite;
    public string ActionScriptName;
}
