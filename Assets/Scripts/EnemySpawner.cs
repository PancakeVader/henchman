﻿using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    public List<EnemyAsset> EnemyList;
    public List<GameObject> SpawnedEnemies;
    public GameObject EnemyPrefab;
    public Transform SpawnLocation;
    private int _runningId;
    private int _enemyCount;

    private void Awake()
    {
        _enemyCount = EnemyList.Count;
        SpawnedEnemies = new List<GameObject>();
    }

    public GameObject SpawnEnemy()
    {
        EnemyAsset spawningEnemy = EnemyList[_enemyCount - 1];
        EnemyList.RemoveAt(_enemyCount - 1);
        _enemyCount--;

        GameObject enemy = Instantiate(EnemyPrefab);
        EnemyBuilder.BuildEnemy(spawningEnemy, enemy);
        enemy.GetComponent<EnemyObject>().Id = _runningId++;
        SpawnedEnemies.Add(enemy);
        ReorderEnemies();
        return enemy;
    }

    private void ReorderEnemies()
    {
        Vector3 offset = new Vector3(1.5f, 0f, 0.1f);

        //We calculate the position for the left most enemy.
        if (SpawnedEnemies != null)
        {
            // ReSharper disable once PossibleLossOfFraction
            Vector3 startingLocation = -(offset * (SpawnedEnemies.Count / 2)) + SpawnLocation.position;

            // Center of even enemies isn't on origo so we add half the offset to center
            // everything properly
            if (SpawnedEnemies.Count % 2 == 0)
                startingLocation += offset / 2;

            for (int i = 0; i < SpawnedEnemies.Count; i++)
            {
                SpawnedEnemies[i].transform.position = startingLocation + (offset * i);
            }
        }
    }


    //Test spawning enemies
    private void Update()
    {
        if(Input.GetKeyDown("w"))
        {
            if (_enemyCount > 0)
                SpawnEnemy();
        }
    }
}
