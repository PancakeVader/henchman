﻿public static class Events
{
    public const int ChangeHealth = 0;
    public const int TableCardsUpdated = 1;


    public const int EnableMouseInteraction = 2;
    public const int DisableMouseInteraction = 3;

    public const int EnableTargetSwitching = 4;
    public const int DisableTargetSwitching = 5;

    public const int ChangeTarget = 6;
    public const int PlayerDied = 7;

}