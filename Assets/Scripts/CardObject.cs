﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// CardObject represents the physical card in the scene.
/// It handels displaying the card's information correctly
/// </summary>
public class CardObject : MonoBehaviour {

    public CardAsset CardData { get; set; }
}