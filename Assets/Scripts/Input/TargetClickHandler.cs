﻿using System.Collections.Generic;
using UnityEngine;


public class TargetClickHandler : MonoBehaviour
{

    public PlayerManager PlayerManager;

    private bool _canSwitchTargets;
    private Ray _rayFromCursor;
    private RaycastHit _hit;

    private void Awake()
    {
        _canSwitchTargets = true;
        EventManager.StartListening(Events.EnableTargetSwitching, EnableTargetSwitching);
        EventManager.StartListening(Events.DisableTargetSwitching, DisableTargetSwitching);
    }

    private void Update()
    {
        if (!_canSwitchTargets) return;

        if (Input.GetMouseButtonDown(0))
        {
            _rayFromCursor = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(_rayFromCursor, out _hit, 100f, LayerMask.GetMask("Enemies")))
            {
                int targetId = _hit.transform.GetComponent<EnemyObject>().Id;
                EventManager.TriggerEvent(Events.ChangeTarget,
                    new Dictionary<int, int> { { Events.ChangeTarget, targetId } });
            }
        }
    }

    private void EnableTargetSwitching(Dictionary<int, int> eventParams)
    {
        _canSwitchTargets = true;
    }

    private void DisableTargetSwitching(Dictionary<int, int> eventParams)
    {
        _canSwitchTargets = false;
    }
}

