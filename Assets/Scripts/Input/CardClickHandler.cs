﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class handles what happens when player clicks on the cards
/// and calls other classes to handle the logic after the click
/// </summary>
public class CardClickHandler : MonoBehaviour {

    private bool _previewActive;
    private bool _canInteractWithCards;

    private Ray _rayFromCursor;
    private RaycastHit _hit;

    public PreviewCard CardPreviewer;

    private void Awake()
    {
        _canInteractWithCards = true;
        EventManager.StartListening(Events.EnableMouseInteraction, EnableCardInteraction);
        EventManager.StartListening(Events.DisableMouseInteraction, DisableCardInteraction);
    }

    private void Update()
    {
        if (_canInteractWithCards)
        {
            HandleClicking();
        }
    }

    private void HandleClicking()
    {
        // Check if player wanted to review the car
        if (!_previewActive && Input.GetMouseButtonDown(1))
        {
            _rayFromCursor = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(_rayFromCursor, out _hit, 100f, LayerMask.GetMask("Cards")))
            {
                CardPreviewer.StartPreview(_hit.transform);
                _previewActive = true;
            }
        }
        // Clicking on a card moves it from hand to table OR from table to hand
        else if (!_previewActive && Input.GetMouseButtonDown(0))
        {
            _rayFromCursor = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(_rayFromCursor, out _hit, 100f, LayerMask.GetMask("Cards")))
            {
                CardContainer parent = _hit.transform.GetComponentInParent<CardContainer>();
                parent.MoveCard(_hit.transform.gameObject);
            }
        }
        else if (_previewActive)
        {
            //Any mouse click while previewing ends the preview
            if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))
            {
                CardPreviewer.EndPreview();
                _previewActive = false;
            }
        }
    }

    private void EnableCardInteraction(Dictionary<int, int> eventParams)
    {
        _canInteractWithCards = true;
    }

    private void DisableCardInteraction(Dictionary<int, int> eventParams)
    {
        _canInteractWithCards = false;
    }
}
