﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// CardBuilder handles building the actual GameObject
/// from CardAssets.
/// </summary>
public class CardBuilder : MonoBehaviour {

    [SerializeField]
    private GameObject _cardPrefab;

    private static CardBuilder _cardBuilder;

    public static CardBuilder Instance
    {
        get
        {
            if (!_cardBuilder)
            {
                _cardBuilder = FindObjectOfType(typeof(CardBuilder)) as CardBuilder;

                if (!_cardBuilder)
                {
                    Debug.LogError("There needs to be one active CardBuilder script in the scene");
                }

            }
            return _cardBuilder;
        }
    }


    // Creates a Card object from given Asset and Gameobject
    public GameObject BuildCard(CardAsset cardAsset)
    {
        GameObject newCard = Instantiate(_cardPrefab);
        newCard.name = cardAsset.name;
        newCard.GetComponent<CardObject>().CardData = cardAsset;

        // We take a type reference of the type named in ActionScriptName
        System.Type type = System.Type.GetType(cardAsset.ActionScriptName);

        // Throws a warning if Unity can't find the script named in ActionScriptName
        if (type != null)
        {
            newCard.AddComponent(type);
        }
        else
        {
            Debug.LogWarning("Action Script not found. Default script used");
            newCard.AddComponent(System.Type.GetType("Focus"));
        }

        return newCard;
    }
}